<?php
/*
 * Plugin Name:	Veda Demo Importer
 * URI: 	http://wedesignthemes.com/plugins/veda-demo-importer
 * Description: A simple wordpress plugin designed to import demo contents for <strong>VEDA</strong> theme 
 * Version: 	1.6
 * Author: 		DesignThemes 
 * Text Domain: veda-importer
 * Author URI:	http://themeforest.net/user/designthemes
 */
if (! class_exists ( 'DTVedaImporter' )) {
	class DTVedaImporter {

		function __construct() {

			add_action('wp_ajax_veda_ajax_importer',  array( $this, 'veda_ajax_importer' ) );
		}

		function veda_ajax_importer() {
			require_once plugin_dir_path ( __FILE__ ) . 'importer/import.php';
			die();
		}
	}

	new DTVedaImporter();
}