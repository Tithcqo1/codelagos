<?php
/*
Plugin Name: Dotstudio Responsive Countdown
Plugin URI: http://dotstudio.co.uk/codecanyon/countdown/demo/
Description: HTML5 Canvas based responsive countdown
Author: Chrisdean
Version: 1.0
Author URI: http://dotstudio.co.uk/
License: You should have purchased a license from http://codecanyon.net/
*/

$ui_names = Array("ui_div_id", "ui_set_id", "ui_pan_id", "ui_fillStyleSymbol1", "ui_fillStyleSymbol2", "ui_fillStylesPanel_g1_1", "ui_fillStylesPanel_g2_1", "ui_fillStylesPanel_g1_2", "ui_fillStylesPanel_g2_2", "ui_text_color", "ui_text_glow", "ui_groups_spacing", "ui_spacer", "ui_font_to_digit_ratio", "ui_labels_space", "ui_days_long", "ui_days_short", "ui_hours_long", "ui_hours_short", "ui_mins_long", "ui_mins_short", "ui_secs_long", "ui_secs_short","ui_show_dd","ui_show_hh","ui_show_mm","ui_show_ss","ui_show_labels", "ui_type3d", "ui_day_digits", "ui_max_height", "ui_min_f_size", "ui_max_f_size", "ui_f_family", "ui_text_blur", "ui_hh_val", "ui_mm_val", "ui_ss_val", "ui_year_val", "ui_month_val", "ui_day_val", "ui_time_zone", "ui_server_now_date", "ui_use_now_offset", "ui_type_offset", "ui_value_offset", "ui_use_custom_update", "ui_new_custom_state", "ui_complete_handler", "ui_target_future");
$ui_default = Array('set_unique_name', 1, 2, "#A61C2A", "#ffffff", "#A8A8A8", "#505082", "#DEDEDE", "#14143C", "#000000", "#888888", 3, "circles", 0.15, 1.2, "DAYS", "DD", "HOURS", "HH", "MINUTES", "MM", "SECONDS", "SS", 1, 1, 1, 1, 1, "single", 2, 150, 9, 30, "Verdana", 2, 0, 0, 0, date('Y', strtotime('+1 year')), 1, 1, 0, 0, 0, 1000, 30, "false", "", "function(){alert('Ready!')}", 1);
$ui_els = Array("text", "dsrc_select", "dsrc_select", "dsrc_color", "dsrc_color", "dsrc_color", "dsrc_color", "dsrc_color", "dsrc_color", "dsrc_color", "dsrc_color", "dsrc_slide", "dsrc_select", "dsrc_slide", "dsrc_slide", "dsrc_text", "dsrc_text", "dsrc_text", "dsrc_text", "dsrc_text", "dsrc_text", "dsrc_text", "dsrc_text", "dsrc_check", "dsrc_check", "dsrc_check", "dsrc_check", "dsrc_check", "dsrc_select", "dsrc_slide", "dsrc_slide", "dsrc_text", "dsrc_text", "dsrc_text", "dsrc_slide", "dsrc_slide", "dsrc_slide", "dsrc_slide", "", "", "", "dsrc_select", "dsrc_check", "dsrc_select", "dsrc_select", "dsrc_text");
$ui_classes = Array("ui_min_f_size"=>"half_size", "ui_max_f_size"=>"half_size");
$ui_types = Array("String", "Number", "Number", "String", "String", "String", "String", "String", "String", "String", "String", "Number", "String", "Number", "Number", "String", "String", "String", "String", "String", "String", "String", "String", "String", "String", "String", "String", "Number", "Number", "Number", "Number", "Number", "Number", "String", "Number", "Number", "Number", "Number", "Number", "Number", "Number", "Number", "String", "Number", "Number", "Number");
$ui_els_values = Array("ui_use_now_offset"=>Array(0, 1, -1), "ui_type_offset"=>Array(1000, 60000, 3600000, 86400000), "ui_server_now_date"=>"yes", "ui_time_zone"=>Array(1000,13,12,11,10,9,8,7,6,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12), "ui_text_blur"=>Array(0,8,2), "ui_show_labels"=>"yes", "ui_show_ss"=>"yes", "ui_show_mm"=>"yes", "ui_show_hh"=>"yes", "ui_show_dd"=>"yes", "ui_set_id"=>Array(0,1), "ui_pan_id"=>Array(0,1,2), "ui_groups_spacing"=>Array(0,5,1), "ui_spacer"=>Array("none","squares","circles"), "ui_font_to_digit_ratio"=>Array(0.1,0.5,0.05),"ui_labels_space"=>Array(1,1.3,0.05), "ui_type3d"=>Array("single","group"), "ui_day_digits"=>Array(1,10,1), "ui_max_height"=>Array(50,300,1), "ui_hh_val"=>Array(0,23,1), "ui_mm_val"=>Array(0,59,1), "ui_ss_val"=>Array(0,59,1));
$ui_els_labels = Array("ui_use_now_offset"=>Array("do not use", "now plus", "now minus"), "ui_type_offset"=>Array("seconds", "minutes", "hours", "days"), "ui_server_now_date"=>"Server Time", "ui_time_zone"=>Array("visitor's",13,12,11,10,9,8,7,6,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12), "ui_show_labels"=>"Labels", "ui_show_ss"=>"Seconds Group", "ui_show_mm"=>"Minutes Group", "ui_show_hh"=>"Hours Group", "ui_show_dd"=>"Days Group", "ui_set_id"=>Array("Oswald", "Federation"), "ui_pan_id"=>Array("Thin Rounded", "Wide Squared", "Wide Rounded"), "ui_spacer"=>Array("none","squares","circles"), "ui_type3d"=>Array("single","group"));
$ui_help = Array("", "Typeface of the digits (set_id)", "Panel figure behind each digit (pan_id)", "Digit color of most right group (fillStyleSymbol1)", "Digit color of alternating group (fillStyleSymbol2)", "Start gradient color of most right digits group (fillStylesPanel_g1_1)", "Start gradient color of alternating digits group (fillStylesPanel_g2_1)", "End gradient color of most right digits group (fillStylesPanel_g1_2)", "End gradient color of alternating digits group (fillStylesPanel_g2_2)", "ui_text_color", "ui_text_glow", "ui_groups_spacing", "ui_spacer", "ui_font_to_digit_ratio", "ui_labels_space", "ui_days_long", "ui_days_short", "ui_hours_long", "ui_hours_short", "ui_mins_long", "ui_mins_short", "ui_secs_long", "ui_secs_short","ui_show_dd","ui_show_hh","ui_show_mm","ui_show_ss","ui_show_labels", "ui_type3d", "ui_day_digits", "ui_max_height", "ui_min_f_size", "ui_max_f_size", "ui_f_family", "ui_text_blur", "ui_hh_val", "ui_mm_val", "ui_ss_val", "ui_year_val", "ui_month_val", "ui_day_val", "ui_time_zone", "ui_server_now_date", "ui_use_now_offset", "ui_type_offset", "ui_value_offset");

$use_color = "aaffaa";
$dont_use_color = "888888";
//-------------------------------------------------------------------------------------------------
//activate    
function dsrc_countdown_activation() {
}
register_activation_hook(__FILE__, 'dsrc_countdown_activation');
//deactivate
function dsrc_countdown_deactivation() {
}
register_deactivation_hook(__FILE__, 'dsrc_countdown_deactivation');    
//-------------------------------------------------------------------------------------------------
$dsrc_plugins_url = plugins_url('js/jquery.responsive_countdown.min.js', __FILE__);
if(stripos(home_url(), 'https://') !== false) {
  $dsrc_plugins_url = str_ireplace('http://', 'https://', $dsrc_plugins_url);
}
//front end scripts js files
add_action('wp_enqueue_scripts', 'dsrc_scripts');
function dsrc_scripts() {
	global $dsrc_plugins_url;
	wp_register_script('dsrc_countdown_core', $dsrc_plugins_url, array("jquery"));
  wp_enqueue_script('jquery');
  wp_enqueue_script('dsrc_countdown_core');
}
function dsrc_my_enqueue() {
    global $post, $dsrc_plugins_url;
    if (!$post) return;
		if($post->post_type == 'dsrc_countdown_type'){
			wp_register_script('dsrc_countdown_core', $dsrc_plugins_url, array("jquery"));
		  wp_enqueue_script('dsrc_countdown_core');
		  wp_enqueue_script('jquery-ui-core');
		  //wp_enqueue_script('jquery-ui-widget');
		  //wp_enqueue_script('jquery-ui-mouse');
		  wp_enqueue_script('jquery-ui-slider');
		  //wp_enqueue_script('jquery-ui-tooltip');
		  wp_enqueue_script('jquery-ui-datepicker');
		  
		  wp_enqueue_style('dsrc-admin-ui-css', plugins_url('ui/css/ui-theme/jquery-ui-1.10.4.custom.min.css', __FILE__), false);
		  wp_enqueue_style('dsrc-admin-css', plugins_url('ui/css/dsrc_admin.css', __FILE__), false);
		  wp_register_script('dsrc_color_js', plugins_url('ui/jscolor/jscolor.js', __FILE__),array("jquery"));
		  wp_enqueue_script('dsrc_color_js');
		}
}
//back end scripts js files
add_action('admin_enqueue_scripts', 'dsrc_my_enqueue');
//-------------------------------------------------------------------------------------------------
//removes quick edit, view and trash from custom post type list
function remove_action_links( $actions ) {
	global $post;
  if( $post->post_type == 'dsrc_countdown_type' ) {
	unset($actions['inline hide-if-no-js']);
	unset( $actions['view'] );
	unset( $actions['trash'] );	}
  return $actions;
}
//remove slug box and other post related boxes
function hide_boxes() {
    global $post;
    if (!$post) return;
		if($post->post_type == 'dsrc_countdown_type'){
        echo "<script type='text/javascript'>
            jQuery(document).ready(function($) {
                jQuery('#edit-slug-box, #minor-publishing-actions, #visibility, .num-revisions, .curtime, .misc-pub-post-status').hide();
            });
            </script>
        ";
    }
}
if (is_admin()) {
	add_filter('post_row_actions','remove_action_links',10,2);
	add_action('admin_head', 'hide_boxes'  );
}
add_filter('widget_text', 'do_shortcode');
//-------------------------------------------------------------------------------------------------
//create shortcode
add_shortcode("dsrc", "dsrc_countdown_display_countdown");
function dsrc_countdown_display_countdown($atts, $content = null) {
	global $ui_names, $ui_default;
  
  extract(shortcode_atts(array('id' => '', ), $atts, "dsrc"));//get ID
  //take data by ID
	$countdown_options = get_post_meta($id, "_dsrc_countdown_options", true);
	$countdown_options = str_replace("\'", "'",  $countdown_options);//ugly
  //$countdown_options = str_replace("\"", '"',  $countdown_options);//ugly
	$countdown_options = ($countdown_options != '') ? json_decode($countdown_options) : array();


  for($i=0;$i<count($ui_names);$i++){
    if($i<count($countdown_options))
      $$ui_names[$i] = $countdown_options[$i];
    else{
      $$ui_names[$i] = $ui_default[$i];
    }
    if(array_key_exists(strtolower($ui_names[$i]),$atts)){
      $$ui_names[$i] = $atts[strtolower($ui_names[$i])];
    }
  }



	//HEX to RGB
	if(strlen($ui_fillStyleSymbol1)>6) list($r1, $g1, $b1) = sscanf($ui_fillStyleSymbol1, "#%02x%02x%02x"); else list($r1, $g1, $b1) = sscanf($ui_fillStyleSymbol1, "%02x%02x%02x");
	if(strlen($ui_fillStyleSymbol2)>6) list($r2, $g2, $b2) = sscanf($ui_fillStyleSymbol2, "#%02x%02x%02x"); else list($r2, $g2, $b2) = sscanf($ui_fillStyleSymbol2, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g1_1)>6) list($rg11, $gg11, $bg11) = sscanf($ui_fillStylesPanel_g1_1, "#%02x%02x%02x"); else list($rg11, $gg11, $bg11) = sscanf($ui_fillStylesPanel_g1_1, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g2_1)>6) list($rg21, $gg21, $bg21) = sscanf($ui_fillStylesPanel_g2_1, "#%02x%02x%02x"); else list($rg21, $gg21, $bg21) = sscanf($ui_fillStylesPanel_g2_1, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g1_2)>6) list($rg12, $gg12, $bg12) = sscanf($ui_fillStylesPanel_g1_2, "#%02x%02x%02x"); else list($rg12, $gg12, $bg12) = sscanf($ui_fillStylesPanel_g1_2, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g2_2)>6) list($rg22, $gg22, $bg22) = sscanf($ui_fillStylesPanel_g2_2, "#%02x%02x%02x"); else list($rg22, $gg22, $bg22) = sscanf($ui_fillStylesPanel_g2_2, "%02x%02x%02x");
	if(strlen($ui_text_color)>6) list($rt, $gt, $bt) = sscanf($ui_text_color, "#%02x%02x%02x"); else list($rt, $gt, $bt) = sscanf($ui_text_color, "%02x%02x%02x");
	if(strlen($ui_text_glow)>6) list($rg, $gg, $bg) = sscanf($ui_text_glow, "#%02x%02x%02x"); else list($rg, $gg, $bg) = sscanf($ui_text_glow, "%02x%02x%02x");
	
	$additional_js = "/*empty*/";
	if($content == null) $content= "\"".$ui_year_val."/".$ui_month_val."/".$ui_day_val." ".$ui_hh_val.":".$ui_mm_val.":".$ui_ss_val."\"";
	else $content= "\"".$content."\"";
	$time_zone = $ui_time_zone;
	if($time_zone==1000){
		$time_zone = "time_zone";
		$additional_js = "td = new Date(); time_zone = td.getTimezoneOffset()/(-60);";
	}
	
	$server_now_date = "";
	if($ui_server_now_date!=""){
		$server_now_date = date("Y/m/d H:i:s");
	}


	if($ui_use_now_offset!=0){
		$msOffset = $ui_use_now_offset*$ui_type_offset*$ui_value_offset;
		if($msOffset!=0) $msOffset+=1000;
		$time_zone = "time_zone";
		$content = "target_date";
		$additional_js = "target_date = new Date();target_date = new Date(target_date.getTime() + ".$msOffset.");time_zone = target_date.getTimezoneOffset()/(-60);target_date = target_date.toDateString()+' '+target_date.toTimeString();";
		$server_now_date = "";
	}


	$complete_handling = "";
	if($ui_complete_handler != "")
	$complete_handling = ',complete:'.$ui_complete_handler;


		
	//the HTML element	
  $html  =  '<div id="'.$ui_div_id.'" style="position: relative; width: 100%; height: 100px;"></div>';
	//the javascript
	$html  .= "\n".'<script type=\'text/javascript\'>';
	$html  .= "\n".'/* <![CDATA[ */';
	$html  .= "\n".'jQuery(function() {';
	$html  .= $additional_js;
	$html  .= "\n".$ui_div_id.' = jQuery("#'.$ui_div_id.'").ResponsiveCountdown({';
	$html  .= "\n". 'use_custom_update:'.$ui_use_custom_update.',new_custom_state: "'.$ui_new_custom_state.'",';
	$html  .= "\n". 'target_date:'.$content.',server_now_date: "'.$server_now_date.'",';
	$html  .= "\n". 'time_zone:'.$time_zone.',target_future:'.$ui_target_future.',';
	$html  .= "\n". 'set_id:'.$ui_set_id.',pan_id:'.$ui_pan_id.',day_digits:'.$ui_day_digits.',';
	$html  .= "\n". 'fillStyleSymbol1:"rgba('.$r1.', '.$g1.', '.$b1.', 1)",';
	$html  .= "\n". 'fillStyleSymbol2:"rgba('.$r2.', '.$g2.', '.$b2.', 1)",';
	$html  .= "\n". 'fillStylesPanel_g1_1:"rgba('.$rg11.', '.$gg11.', '.$bg11.', 1)",';
	$html  .= "\n". 'fillStylesPanel_g1_2:"rgba('.$rg12.', '.$gg12.', '.$bg12.', 1)",';
	$html  .= "\n". 'fillStylesPanel_g2_1:"rgba('.$rg21.', '.$gg21.', '.$bg21.', 1)",';
	$html  .= "\n". 'fillStylesPanel_g2_2:"rgba('.$rg22.', '.$gg22.', '.$bg22.', 1)",';
	$html  .= "\n". 'text_color:"rgba('.$rt.', '.$gt.', '.$bt.', 1)",';
	$html  .= "\n". 'text_glow:"rgba('.$rg.', '.$gg.', '.$bg.', 1)",';
	$html  .= "\n". 'show_ss:'.$ui_show_ss.',show_mm:'.$ui_show_mm.',';
	$html  .= "\n". 'show_hh:'.$ui_show_hh.',show_dd:'.$ui_show_dd.',';
	$html  .= "\n". 'f_family:"'.$ui_f_family.'",show_labels:'.$ui_show_labels.',';
	$html  .= "\n". 'type3d:"'.$ui_type3d.'",max_height:'.$ui_max_height.',';
	$html  .= "\n". 'days_long:"'.$ui_days_long.'",days_short:"'.$ui_days_short.'",';
	$html  .= "\n". 'hours_long:"'.$ui_hours_long.'",hours_short:"'.$ui_hours_short.'",';
	$html  .= "\n". 'mins_long:"'.$ui_mins_long.'",mins_short:"'.$ui_mins_short.'",';
	$html  .= "\n". 'secs_long:"'.$ui_secs_long.'",secs_short:"'.$ui_secs_short.'",';
	$html  .= "\n". 'min_f_size:'.$ui_min_f_size.',max_f_size:'.$ui_max_f_size.',';
	$html  .= "\n". 'spacer:"'.$ui_spacer.'",groups_spacing:'.$ui_groups_spacing.',text_blur:'.$ui_text_blur.',';
	$html  .= "\n". 'font_to_digit_ratio:'.$ui_font_to_digit_ratio.',labels_space:'.$ui_labels_space;
	$html  .= $complete_handling;
	$html  .= "\n". '});';
	$html  .= "\n". '});';	
	$html  .= "\n". '/* ]]> */';
	$html  .= "\n". '</script>';
  return $html;
}
//-------------------------------------------------------------------------------------------------
//custom post
add_action( 'init', 'register_dsrc_countdown' );
function register_dsrc_countdown() {
	$labels = array(
		'name' => _x( 'Countdowns', 'countdown' ),
		'singular_name' => _x( 'Countdown', 'countdown' ),
		'add_new' => _x( 'Add New', 'countdown' ),
		'add_new_item' => _x( 'Add New Countdown', 'countdown' ),
		'edit_item' => _x( 'Edit Countdown', 'countdown' ),
		'new_item' => _x( 'New Countdown', 'countdown' ),
		'view_item' => _x( 'View Countdown', 'countdown' ),
		'search_items' => _x( 'Search Countdowns', 'countdown' ),
		'not_found' => _x( 'No countdowns found', 'countdown' ),
		'not_found_in_trash' => _x( 'No countdowns found in Trash', 'countdown' ),
		'parent_item_colon' => _x( 'Parent Countdown:', 'countdown' ),
		'menu_name' => _x( 'Countdowns', 'countdown' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'description' => 'List of countdowns.',
		'menu_icon' => plugins_url('ui/images/cp_icon.jpg', __FILE__),
		'supports' => array( 'title', /*'custom-fields'*/ ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post'
	);
	register_post_type( 'dsrc_countdown_type', $args );
}
//-------------------------------------------------------------------------------------------------
//force one column
function so_screen_layout_columns( $columns ) {
    $columns['post'] = 1;
    return $columns;
}
add_filter( 'screen_layout_columns', 'so_screen_layout_columns' );

function so_screen_layout_post() {
    return 1;
}
add_filter( 'get_user_option_screen_layout_dsrc_countdown_type', 'so_screen_layout_post' );
//-------------------------------------------------------------------------------------------------
//custom post options UI
add_action('add_meta_boxes', 'dsrc_countdown_meta_box');
function dsrc_countdown_meta_box() {
	add_meta_box("dsrc-countdown-options", "Countdown Options", "dsrc_countdown_view_box", "dsrc_countdown_type", "normal");
}
function dsrc_countdown_view_box() {
	global $post, $ui_names, $ui_default, $ui_types, $ui_els_values, $ui_els_labels, $ui_help, $ui_classes, $ui_els, $use_color, $dont_use_color;
	
	function build_onchange_color($id){
		return $id.' = \'rgba(\'+Math.round(this.color.rgb[0]*255)+\', \'+Math.round(this.color.rgb[1]*255)+\', \'+Math.round(this.color.rgb[2]*255)+\', 1)\'; set_color(\''.$id.'\', '.$id.');';
	}
	
	function handle_slide($dsrc_id, $option_name, $option_value){
		global $ui_hh_val;
		if($option_name == "hh_val" || $option_name == "mm_val" || $option_name == "ss_val"){
			//return 'console.log(jQuery("#ui_hh_val_slide").slider("option", "value")+" "+jQuery("#ui_mm_val_slide").slider("option", "value")+" "+jQuery("#ui_ss_val_slide").slider("option", "value"));';
			if($option_name == "hh_val") $hours = $option_value; else $hours = 'jQuery("#ui_hh_val_slide").slider("option", "value")';
			if($option_name == "mm_val") $mins  = $option_value; else $mins  = 'jQuery("#ui_mm_val_slide").slider("option", "value")';
			if($option_name == "ss_val") $secs =  $option_value; else $secs  = 'jQuery("#ui_ss_val_slide").slider("option", "value")';
			$day1 = 'jQuery("#ui_date_picker").datepicker("getDate").getDate()';
			$month1 = 'Number(jQuery("#ui_date_picker").datepicker("getDate").getMonth() + 1)';
			$year1 = 'jQuery("#ui_date_picker").datepicker("getDate").getFullYear()';
			return 'jQuery("#ui_date_picker").val('.$year1.'+"/"+'.$month1.'+"/"+'.$day1.'+" "+'.$hours.'+":"+'.$mins.'+":"+'.$secs.');if(!use_now)'.$dsrc_id.'.set_options(["target_date", '.$year1.'+"/"+'.$month1.'+"/"+'.$day1.'+" "+'.$hours.'+":"+'.$mins.'+":"+'.$secs.'])';
		}
		return $dsrc_id.'.set_options(["'.$option_name.'", '.$option_value.'])';
	}

	function create_element($el_id, $el_value){
		global $ui_els, $ui_names, $ui_types, $ui_els_values, $ui_els_labels, $ui_help, $ui_classes, $use_color, $dont_use_color;
		
		if($ui_els[$el_id] == "dsrc_color"){
			return '<input title="'.$ui_help[$el_id].'" type="text" id="'.$ui_names[$el_id].'" name="'.$ui_names[$el_id].'" value="'.$el_value.'" class="dsrc_color color {pickerClosable:true}" onchange="'.build_onchange_color(substr($ui_names[$el_id], 3)).'" />';
		}
		else if($ui_els[$el_id] == "dsrc_select"){
			$ui_class = '';
			if(array_key_exists ( $ui_names[$el_id] , $ui_classes )){
				$ui_class = $ui_classes[$ui_names[$el_id]];
			}
			$html  = '<select title="'.$ui_help[$el_id].'" class="dsrc_select '.$ui_class.'" data-type="'.$ui_types[$el_id].'" data-select_option="'.substr($ui_names[$el_id], 3).'" id="'.$ui_names[$el_id].'" name="'.$ui_names[$el_id].'">';
			for($i=0; $i<count($ui_els_values[$ui_names[$el_id]]); $i++){
				if($el_value == $ui_els_values[$ui_names[$el_id]][$i])
					$html .= '<option value="'.$ui_els_values[$ui_names[$el_id]][$i].'" selected>'.$ui_els_labels[$ui_names[$el_id]][$i].'</option>';
				else
					$html .= '<option value="'.$ui_els_values[$ui_names[$el_id]][$i].'">'.$ui_els_labels[$ui_names[$el_id]][$i].'</option>';
			}
			$html .= '</select>';
			return $html;
		}
		else if($ui_els[$el_id] == "dsrc_check"){
		  $el_value ? $ui_checked = " checked" : $ui_checked = "";
			return '<input title="'.$ui_help[$el_id].'" class="dsrc_check" data-check_option="'.substr($ui_names[$el_id], 3).'" id="'.$ui_names[$el_id].'" type="checkbox" name="'.$ui_names[$el_id].'" value="'.$ui_els_values[$ui_names[$el_id]].'"'.$ui_checked.' />&nbsp;'.$ui_els_labels[$ui_names[$el_id]];
		}
		else if($ui_els[$el_id] == "dsrc_text"){
			$ui_class = '';
			if(array_key_exists ( $ui_names[$el_id] , $ui_classes )){
				$ui_class = $ui_classes[$ui_names[$el_id]];
			}
			return '<input title="'.$ui_help[$el_id].'" class="dsrc_text '.$ui_class.'" data-text_option="'.substr($ui_names[$el_id], 3).'" id="'.$ui_names[$el_id].'" type="text" name="'.$ui_names[$el_id].'" value="'.$el_value.'" />';
		}
		else if($ui_els[$el_id] == "dsrc_slide"){
			return '<div id="'.$ui_names[$el_id].'_slide" style="width: 95%; margin: 8px 0px 0px 0px;"></div><input type="hidden" name="'.$ui_names[$el_id].'" id="'.$ui_names[$el_id].'" value="'.$el_value. '" />';
		}
	}
	
	$countdown_options = get_post_meta($post->ID, "_dsrc_countdown_options", true);
	$countdown_options = str_replace("\'", "'",  $countdown_options);//ugly
	$countdown_options = ($countdown_options != '') ? json_decode($countdown_options) : $ui_default;
	
	for($i=0;$i<count($ui_names);$i++){
		if($i<count($countdown_options))
			$$ui_names[$i] = $countdown_options[$i];
		else
			$$ui_names[$i] = $ui_default[$i];
	}
  
	if(strlen($ui_fillStyleSymbol1)>6) list($r1, $g1, $b1) = sscanf($ui_fillStyleSymbol1, "#%02x%02x%02x"); else list($r1, $g1, $b1) = sscanf($ui_fillStyleSymbol1, "%02x%02x%02x");
	if(strlen($ui_fillStyleSymbol2)>6) list($r2, $g2, $b2) = sscanf($ui_fillStyleSymbol2, "#%02x%02x%02x"); else list($r2, $g2, $b2) = sscanf($ui_fillStyleSymbol2, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g1_1)>6) list($rg11, $gg11, $bg11) = sscanf($ui_fillStylesPanel_g1_1, "#%02x%02x%02x"); else list($rg11, $gg11, $bg11) = sscanf($ui_fillStylesPanel_g1_1, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g2_1)>6) list($rg21, $gg21, $bg21) = sscanf($ui_fillStylesPanel_g2_1, "#%02x%02x%02x"); else list($rg21, $gg21, $bg21) = sscanf($ui_fillStylesPanel_g2_1, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g1_2)>6) list($rg12, $gg12, $bg12) = sscanf($ui_fillStylesPanel_g1_2, "#%02x%02x%02x"); else list($rg12, $gg12, $bg12) = sscanf($ui_fillStylesPanel_g1_2, "%02x%02x%02x");
	if(strlen($ui_fillStylesPanel_g2_2)>6) list($rg22, $gg22, $bg22) = sscanf($ui_fillStylesPanel_g2_2, "#%02x%02x%02x"); else list($rg22, $gg22, $bg22) = sscanf($ui_fillStylesPanel_g2_2, "%02x%02x%02x");
	if(strlen($ui_text_color)>6) list($rt, $gt, $bt) = sscanf($ui_text_color, "#%02x%02x%02x"); else list($rt, $gt, $bt) = sscanf($ui_text_color, "%02x%02x%02x");
	if(strlen($ui_text_glow)>6) list($rg, $gg, $bg) = sscanf($ui_text_glow, "#%02x%02x%02x"); else list($rg, $gg, $bg) = sscanf($ui_text_glow, "%02x%02x%02x");
	
	$additional_js = "";
	$content= "\"".$ui_year_val."/".$ui_month_val."/".$ui_day_val." ".$ui_hh_val.":".$ui_mm_val.":".$ui_ss_val."\"";
	$time_zone = $ui_time_zone;
	if($time_zone==1000){
		$time_zone = "time_zone";
		$additional_js = "target_date = new Date(); time_zone = target_date.getTimezoneOffset()/(-60);";
	}
	
	$server_now_date = "";
	if($ui_server_now_date!=""){
		//$server_now_date = date("Y/m/d H:i:s");
		$time_zone = "time_zone";
		$additional_js = "target_date = new Date();time_zone = target_date.getTimezoneOffset()/(-60)+hDiff;";
	}

	if($ui_use_now_offset!=0){
		$msOffset = $ui_use_now_offset*$ui_type_offset*$ui_value_offset;
		if($msOffset!=0) $msOffset+=1000;
		$time_zone = "time_zone";
		$content = "target_date";
		$additional_js = "target_date = new Date();target_date = new Date(target_date.getTime() + ".$msOffset.");time_zone = target_date.getTimezoneOffset()/(-60);target_date = target_date.toDateString()+' '+target_date.toTimeString();";
		$server_now_date = "";
	}
	
	echo '<hr class="dsrc_hr" id="back_to_top" />';
  echo '<div id="'.$ui_div_id.'" style="position: relative; width: 100%; height: 100px;"></div>';
	echo '<script type=\'text/javascript\'>';
	echo '/* <![CDATA[ */';
	echo 'jQuery(function() {';
?>
	//getting difference in hours between local and server time
	var serverDate = new Date("<?php echo date("Y/m/d H:i:s") ?>");
	var now = new Date();
	var msDiff = serverDate - now;
	var hDiff = Math.round(msDiff/3600000);
	var use_now = <?php if($ui_use_now_offset!=0) echo "true"; else echo "false" ?>;
<?php	
	echo $additional_js;
	echo $ui_div_id.' = jQuery("#'.$ui_div_id.'").ResponsiveCountdown({';
	echo 'target_date:'.$content.',paused: false,server_now_date: "'.$server_now_date.'",';
	echo 'time_zone:'.$time_zone.',target_future:false,';
	echo 'set_id:'.$ui_set_id.',pan_id:'.$ui_pan_id.',day_digits:'.$ui_day_digits.',';
	echo 'fillStyleSymbol1:"rgba('.$r1.', '.$g1.', '.$b1.', 1)",';
	echo 'fillStyleSymbol2:"rgba('.$r2.', '.$g2.', '.$b2.', 1)",';
	echo 'fillStylesPanel_g1_1:"rgba('.$rg11.', '.$gg11.', '.$bg11.', 1)",';
	echo 'fillStylesPanel_g1_2:"rgba('.$rg12.', '.$gg12.', '.$bg12.', 1)",';
	echo 'fillStylesPanel_g2_1:"rgba('.$rg21.', '.$gg21.', '.$bg21.', 1)",';
	echo 'fillStylesPanel_g2_2:"rgba('.$rg22.', '.$gg22.', '.$bg22.', 1)",';
	echo 'text_color:"rgba('.$rt.', '.$gt.', '.$bt.', 1)",';
	echo 'text_glow:"rgba('.$rg.', '.$gg.', '.$bg.', 1)",';
	echo 'show_ss:'.$ui_show_ss.',show_mm:'.$ui_show_mm.',';
	echo 'show_hh:'.$ui_show_hh.',show_dd:'.$ui_show_dd.',';
	echo 'f_family:"'.$ui_f_family.'",show_labels:'.$ui_show_labels.',';
	echo 'type3d:"'.$ui_type3d.'",max_height:'.$ui_max_height.',';
	echo 'days_long:"'.$ui_days_long.'",days_short:"'.$ui_days_short.'",';
	echo 'hours_long:"'.$ui_hours_long.'",hours_short:"'.$ui_hours_short.'",';
	echo 'mins_long:"'.$ui_mins_long.'",mins_short:"'.$ui_mins_short.'",';
	echo 'secs_long:"'.$ui_secs_long.'",secs_short:"'.$ui_secs_short.'",';
	echo 'min_f_size:'.$ui_min_f_size.',max_f_size:'.$ui_max_f_size.',';
	echo 'spacer:"'.$ui_spacer.'",groups_spacing:'.$ui_groups_spacing.',text_blur: '.$ui_text_blur.',';
	echo 'font_to_digit_ratio:'.$ui_font_to_digit_ratio.',labels_space:'.$ui_labels_space;
	echo '});';

?>
	jQuery(".dsrc_text").change(function() {
		if(jQuery("#ui_use_now_offset").val()!=0 && jQuery(this).data("text_option") == "value_offset"){
			var msOffset = Number(jQuery("#ui_use_now_offset").val())*Number(jQuery("#ui_type_offset").val())*Number(jQuery("#ui_value_offset").val());
			if(msOffset!=0) msOffset+=1000;
			var target_date = new Date();
			target_date = new Date(target_date.getTime() + msOffset);
			var time_zone = target_date.getTimezoneOffset()/(-60);
			target_date = target_date.toDateString()+' '+target_date.toTimeString();
			<?php echo $ui_div_id?>.set_options(["time_zone", time_zone, "target_date", target_date, "server_now_date", ""]);		
		}
		else{
			<?php echo $ui_div_id?>.set_options([jQuery(this).data("text_option"), jQuery(this).val()]);
		}
	});
	jQuery(".dsrc_check").change(function() {
		if(jQuery(this).data("check_option") == "server_now_date"){
			//simulate server time by changing the timezone according to hour difference between local and server time	
			target_date = new Date(); time_zone = target_date.getTimezoneOffset()/(-60);		
			if(jQuery("#ui_server_now_date").is(":checked")){//do simulation
				if(!use_now)<?php echo $ui_div_id?>.set_options(["time_zone", time_zone+hDiff]);
			}
			else{//use time zone setting
				if(jQuery("#ui_time_zone").val() == 1000){
					if(!use_now)<?php echo $ui_div_id?>.set_options(["time_zone", time_zone]);
				}
				else{
					if(!use_now)<?php echo $ui_div_id?>.set_options(["time_zone", Number(jQuery("#ui_time_zone").val())]);
				}
			}
		}
		else{
			<?php echo $ui_div_id?>.set_options([jQuery(this).data("check_option"), jQuery(this).is(":checked")]);
		}
	});
	jQuery(".dsrc_select").change(function() {
		if(jQuery("#ui_use_now_offset").val()!=0){
			jQuery(".dont_use").css("background-color","#<?php echo $dont_use_color ?>");
			jQuery(".use").css("background-color","#<?php echo $use_color ?>");
			use_now = true;
			var msOffset = Number(jQuery("#ui_use_now_offset").val())*Number(jQuery("#ui_type_offset").val())*Number(jQuery("#ui_value_offset").val());
			if(msOffset!=0) msOffset+=1000;
			var target_date = new Date();
			target_date = new Date(target_date.getTime() + msOffset);
			var time_zone = target_date.getTimezoneOffset()/(-60);
			target_date = target_date.toDateString()+' '+target_date.toTimeString();
			<?php echo $ui_div_id?>.set_options(["time_zone", time_zone, "target_date", target_date, "server_now_date", ""]);
		}
		else{
			jQuery(".use").css("background-color","#<?php echo $dont_use_color ?>");
			jQuery(".dont_use").css("background-color","#<?php echo $use_color ?>");
			use_now = false;
			var day1   = jQuery("#ui_date_picker").datepicker('getDate').getDate();
			var month1 = jQuery("#ui_date_picker").datepicker('getDate').getMonth() + 1;             
			var year1  = jQuery("#ui_date_picker").datepicker('getDate').getFullYear();
			var fullDate = year1 + "/" + month1 + "/" + day1;
			var hh1 = jQuery("#ui_hh_val_slide").slider("option", "value");
			var mm1 = jQuery("#ui_mm_val_slide").slider("option", "value");
			var ss1 = jQuery("#ui_ss_val_slide").slider("option", "value");
			if(jQuery("#ui_time_zone").val() == 1000){
				target_date = new Date(); time_zone = target_date.getTimezoneOffset()/(-60);
			}
			else{
				time_zone = Number(jQuery("#ui_time_zone").val());
			}
			<?php echo $ui_div_id?>.set_options(["target_date", fullDate+' '+hh1+':'+mm1+':'+ss1, "time_zone", time_zone]);
		}
		if(jQuery(this).data("select_option") == "time_zone"){			
			//exception for local timezone
			if(jQuery("#ui_time_zone").val() == 1000){
				target_date = new Date(); time_zone = target_date.getTimezoneOffset()/(-60);
				if(!use_now)<?php echo $ui_div_id?>.set_options(["time_zone", time_zone]);
			}
			else{
				if(!use_now)<?php echo $ui_div_id?>.set_options(["time_zone", Number(jQuery("#ui_time_zone").val())]);
			}
		}
		else{
			<?php echo $ui_div_id?>.set_options([jQuery(this).data("select_option"), jQuery(this).data("type")=="Number"?Number(jQuery(this).val()):jQuery(this).val()]);
		}
	});
	/*
	jQuery( document ).tooltip({
      position: {
        my: "center bottom-20",
        at: "center top"
      }
  });
  */
  jQuery("#ui_date_picker").datepicker({
  		dateFormat: "yy/m/d",
  		defaultDate: "<?php echo $ui_year_val."/".$ui_month_val."/".$ui_day_val ?>", 
			onSelect: function(){
				var day1   = jQuery("#ui_date_picker").datepicker('getDate').getDate();                 
				var month1 = jQuery("#ui_date_picker").datepicker('getDate').getMonth() + 1;             
				var year1  = jQuery("#ui_date_picker").datepicker('getDate').getFullYear();
				jQuery("#ui_year_val").val(year1);
				jQuery("#ui_month_val").val(month1);
				jQuery("#ui_day_val").val(day1);
				var fullDate = year1 + "/" + month1 + "/" + day1;
				//jQuery('#page_output').html(str_output);
				var hh1 = jQuery("#ui_hh_val_slide").slider("option", "value");
				var mm1 = jQuery("#ui_mm_val_slide").slider("option", "value");
				var ss1 = jQuery("#ui_ss_val_slide").slider("option", "value");
				jQuery("#ui_date_picker").val(fullDate+' '+hh1+':'+mm1+':'+ss1);
				if(!use_now){
					<?php echo $ui_div_id?>.set_options(["target_date", fullDate+' '+hh1+':'+mm1+':'+ss1]);
				}
			}
	});
  <?php
  for($i=0;$i<count($ui_els);$i++){
  	if($ui_els[$i] == "dsrc_slide"){
			echo 'jQuery( "#'.$ui_names[$i].'_slide" ).slider({';
			echo 'min: '.$ui_els_values[$ui_names[$i]][0].',';
			echo 'max: '.$ui_els_values[$ui_names[$i]][1].',';
			echo 'step: '.$ui_els_values[$ui_names[$i]][2].',';
			echo 'slide: function( event, ui ) {';
			echo substr($ui_names[$i], 3).' = Number(ui.value);jQuery("#'.$ui_names[$i].'").val('.substr($ui_names[$i], 3).');'.handle_slide($ui_div_id, substr($ui_names[$i], 3), substr($ui_names[$i], 3));
			echo '}';
			echo '}).slider( "value", '.$$ui_names[$i].');';
  	}
  }
  

	echo '});';	
	echo 'function set_color(color_name, color_value){'.$ui_div_id.'.set_options([color_name, color_value]);}';

	echo '/* ]]> */';
	echo '</script>';
	
	// Use nonce for verification
	$html  =  '<input type="hidden" name="dsrc_countdown_box_nonce" value="'. wp_create_nonce(basename(__FILE__)). '" />';


	$html .= '<div class="dsrc_ui_div_cont">';
	$html .= '<div class="dsrc_div_label"><span>Container ID</span></div>';
	$html .= '<div class="dsrc_div_el"><input style="width:100%;" id="ui_div_id" type="text" name="ui_div_id" value="'.$ui_div_id.'" /></div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">ID of DIV container [ui_div_id]<a href="#container_id"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Shortcode</span></div>';
	$html .= '<div class="dsrc_div_label"><span>[dsrc id=\''.$post->ID.'\' /]</span></div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Insert this code in your content <a href="#shortcode"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Tool Max Height</span></div>';
	$html .= '<div class="dsrc_div_label">'.create_element(30, $$ui_names[30]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Maximum height of the tool [ui_max_height]<a href="#max_height"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';


	if($ui_use_now_offset!=0){$color1 = $dont_use_color; $color2 = $use_color;}
	else {$color2 = $dont_use_color; $color1 = $use_color;}
	$html .= '<div class="dsrc_ui_div_cont dont_use" style="background-color:#'.$color1.'">';
	$html .= '<input type="hidden" name="ui_year_val" id="ui_year_val" value="'.$ui_year_val.'" />';
	$html .= '<input type="hidden" name="ui_month_val" id="ui_month_val" value="'.$ui_month_val.'" />';
	$html .= '<input type="hidden" name="ui_day_val" id="ui_day_val" value="'.$ui_day_val.'" />';
	$html .= '<input type="hidden" name="ui_use_custom_update" id="ui_use_custom_update" value="'.$ui_default[46].'" />';
	$html .= '<input type="hidden" name="ui_new_custom_state" id="ui_new_custom_state" value="'.$ui_default[47].'" />';
	$html .= '<div class="dsrc_div_label"><span>Target Date</span></div>';
	$html .= '<div class="dsrc_div_el"><input type="text" style="width: 100%; min-width: 70px; text-align:center;" name="ui_date_picker" id="ui_date_picker" value="'.$ui_year_val.'/'.$ui_month_val.'/'.$ui_day_val.' '.$ui_hh_val.':'.$ui_mm_val.':'.$ui_ss_val.'"></div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Date part (<b>Y/m/d</b> H:i:s)<a href="#target_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Time Zone</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(41, $$ui_names[41]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Time zone of event area [ui_time_zone] <a href="#time_zone"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Use Server Time</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(42, $$ui_names[42]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Gets set with PHP [ui_server_now_date] <a href="#server_now_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';
	
	

	$html .= '<div class="dsrc_ui_div_cont dont_use" style="background-color:#'.$color1.'">';
	$html .= '<div class="dsrc_div_label"><span>Time Hours</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(35, $$ui_names[35]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Hours part (Y/m/d <b>H</b>:i:s) <a href="#target_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Time Minutes</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(36, $$ui_names[36]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Minutes part (Y/m/d H:<b>i</b>:s) <a href="#target_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Time Seconds</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(37, $$ui_names[37]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Seconds part (Y/m/d H:i:<b>s</b>) <a href="#target_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';

	$html .= '<div class="dsrc_ui_div_cont use" style="background-color:#'.$color2.'">';
	$html .= '<div class="dsrc_div_label"><span>Use Now Offset</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(43, $$ui_names[43]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Relative to current time <a href="#relative_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Offset Units</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(44, $$ui_names[44]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Choose units <a href="#relative_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Offset Value</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(45, $$ui_names[45]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Value of offset from current time <a href="#relative_date"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';


	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#dddddd">';
	$html .= '<div class="dsrc_div_label"><span>Digit Color 1</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(3, $$ui_names[3]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Color of rightmost group digit [ui_fillStyleSymbol1] <a href="#fillStyleSymbol1"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Gradient Start 1</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(5, $$ui_names[5]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Panel gradient (middle) [ui_fillStylesPanel_g1_1] <a href="#fillStylesPanel_g1_1"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Gradient End 1</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(7, $$ui_names[7]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Panel gradient (edge) [ui_fillStylesPanel_g1_2] <a href="#fillStylesPanel_g1_2"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';

	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#dddddd">';
	$html .= '<div class="dsrc_div_label"><span>Digit Color 2</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(4, $$ui_names[4]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Alternating digit group color [ui_fillStyleSymbol2] <a href="#fillStyleSymbol1"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Gradient Start 2</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(6, $$ui_names[6]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Panel gradient (middle) [ui_fillStylesPanel_g2_1] <a href="#fillStylesPanel_g1_1"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Gradient End 2</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(8, $$ui_names[8]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Panel gradient (edge) [ui_fillStylesPanel_g2_2] <a href="#fillStylesPanel_g1_2"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';



	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffaaaa">';
	$html .= '<div class="dsrc_div_label"><span>Digits Font</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(1, $$ui_names[1]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">SVG defined digits [ui_set_id] <a href="#set_id"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Panel Behind Digit</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(2, $$ui_names[2]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Panel appearance [ui_pan_id] <a href="#pan_id"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>3D Projection</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(28, $$ui_names[28]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Point of view for the 3D effect [ui_type3d] <a href="#type3d"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';

	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffaaaa">';
	$html .= '<div class="dsrc_div_label"><span>Group Spacing</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(11, $$ui_names[11]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Distance between groups [ui_groups_spacing] <a href="#groups_spacing"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Spacer Icon</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(12, $$ui_names[12]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Use spacer icons [ui_spacer] <a href="#spacer"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Day Digits Number</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(29, $$ui_names[29]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Set number of day digits [ui_day_digits] <a href="#day_digits"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';

	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffffaa">';
	$html .= '<div class="dsrc_div_label"><span>Label Color</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(9, $$ui_names[9]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Color of labels and spacers [ui_text_color] <a href="#text_color"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Label Glow</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(10, $$ui_names[10]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Glow color of labels and spacers [ui_text_glow] <a href="#text_glow"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Label Blur</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(34, $$ui_names[34]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Blur amount of labels and spacers [ui_text_blur] <a href="#text_blur"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';


	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffffaa">';
	$html .= '<div class="dsrc_div_label"><span>Label Size</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(13, $$ui_names[13]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Size of label (% of digit) [ui_font_to_digit_ratio] <a href="#font_to_digit_ratio"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Gap Size</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(14, $$ui_names[14]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Gap from digits (% of label) [ui_labels_space] <a href="#labels_space"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label">&nbsp;</div>';
	$html .= '<div class="dsrc_div_el">&nbsp;</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">&nbsp;</div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';

	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffffaa">';
	$html .= '<div class="dsrc_div_label"><span>Days Long</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(15, $$ui_names[15]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Long caption for days group [ui_days_long] <a href="#days_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Days Short</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(16, $$ui_names[16]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Short caption for days group [ui_days_short] <a href="#days_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Show/Hide</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(23, $$ui_names[23]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Display or hide days group [ui_show_dd] <a href="#show_hide"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';


	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffffaa">';
	$html .= '<div class="dsrc_div_label"><span>Hours Long</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(17, $$ui_names[17]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Long caption for hours group [ui_hours_long] <a href="#hours_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Hours Short</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(18, $$ui_names[18]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Short caption for hours group [ui_hours_short] <a href="#hours_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Show/Hide</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(24, $$ui_names[24]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Display or hide hours group [ui_show_hh] <a href="#show_hide"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';


	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffffaa">';
	$html .= '<div class="dsrc_div_label"><span>Minutes Long</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(19, $$ui_names[19]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Long caption for minutes group [ui_mins_long] <a href="#mins_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Minutes Short</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(20, $$ui_names[20]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Short caption for minutes group [ui_mins_short] <a href="#mins_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Show/Hide</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(25, $$ui_names[25]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Display or hide minutes group [ui_show_mm] <a href="#show_hide"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';


	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffffaa">';
	$html .= '<div class="dsrc_div_label"><span>Seconds Long</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(21, $$ui_names[21]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Long caption for seconds group [ui_secs_long] <a href="#secs_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Seconds Short</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(22, $$ui_names[22]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Short caption for seconds group [ui_secs_short] <a href="#secs_long"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Show/Hide</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(26, $$ui_names[26]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Display or hide seconds group [ui_show_ss] <a href="#show_hide"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';

	$html .= '<div class="dsrc_ui_div_cont" style="background-color:#ffffaa">';
	$html .= '<div class="dsrc_div_label"><span>Show/Hide</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(27, $$ui_names[27]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Control visibility of all labels [ui_show_labels] <a href="#show_hide"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Labels Font</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(33, $$ui_names[33]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Use fonts supported by your theme [ui_f_family] <a href="#f_family"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="dsrc_div_label"><span>Label Size(min/max)</span></div>';
	$html .= '<div class="dsrc_div_el">'.create_element(31, $$ui_names[31]).create_element(32, $$ui_names[32]).'</div>';
	$html .= '<div class="clear"></div>';
	$html .= '<div class="dsrc_note">Label font size [ui_min_f_size][ui_max_f_size] <a href="#f_size"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<div class="clear"></div>';
	$html .= '</div>';



	$html .= '<div class="dsrc_ui_div_cont">';
	$html .= '<div class="dsrc_div_label"><span>Complete Handler</span></div>';
	$html .= '<textarea style="height:120px; width: 290px" id="ui_complete_handler" name="ui_complete_handler">'.$ui_complete_handler.'</textarea>';
	$html .= '<div class="dsrc_note">Javascript function [ui_complete_handler] <a href="#complete"><img src="'.plugins_url('ui/images/more.png', __FILE__).'" /></a></div>';
	$html .= '<input type="hidden" name="ui_target_future" value="1"><div class="clear"></div>';
	$html .= '</div>';





	$html .= '<div class="clear"></div>';

	
	
	echo $html;
	
?>

<hr class="dsrc_hr" id="container_id">
<h4 class="dsrc_h4">Container ID</h4>
<p>When a countdown is created it resides in a div container. This div must be identified with an ID. Use this field to enter unique string value. This value will be also used for the name of the javascript variable that receives the result of countdown creation. When the countdown is used with shortcode, this value can be overridden by the usage of "ui_div_id" variable:</p>

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_div_id='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_div_id='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is a string ("first_countdown", "bd_countdown", etc.)</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="shortcode">
<h4 class="dsrc_h4">Shortcode</h4>
<p>Use this string directly in your posts, or in a PHP file. The simplest way to use the countdown shortcode is to pass its internal id. It is possible to override all options that are already set with the visual editor. Use option name and set new value. To insert a shortcode, simply paste the shortcode in the post or page content editor. If you want to run the shortcodes in sidebar widget, drop in a Text widget and insert the shortcode in the Text box.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="max_height">
<h4 class="dsrc_h4">Tool Max Height</h4>
<p>The tool resizes according to available width. If you put it in container that spans all browser width it is possible that the countdown will look too big on large resolutions. Setting this parameter will restrict its size.</p>

<p>To override this option set with the visual editor use ui_max_height shortcode attribute:</p>
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_max_height='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_max_height='new_value' ]");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is of integer type.</p>

<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="target_date">
<h4 class="dsrc_h4">Target Date</h4>
<p>Use the datepicker tool to select target date. Hours, minutes and seconds can be set using the corresponding sliders. The target date will not be taken into account if the offset method to set target date/time is used.</p>

<p>To override target date set with the visual editor you need to set content for the shortcode. Do not use quotes:</p>
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_use_now_offset='0' /]new_value[/dsrc]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_use_now_offset='0' ]new_value[/dsrc]");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> can be string in Y/m/d H:i:s format.</p>
<p>Note: You will notice that I used 2 attributes. Second attribute is used to make sure that "now offset" functionality is not used for target date setting.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="time_zone">
<h4 class="dsrc_h4">Time Zone</h4>
<p>You will need to use this setting if the target event takes place in a selected location. For example you want to create a countdown that shows remaining time to the first World Cup 2014 soccer game. Since the event is going to happen in Brazil you need to put the corresponding time zone setting, so site visitors from around the world will see same remaining time, which will be calculated based upon their local current time and the time zone of the event. You can use another setting when you for example want to show remaining time till Christmass. That is the "visitor's" setting. Christmass will take place in different times around the globe, but when each visitor calculates remaining time against his own time zone it will show correctly for them.</p>
<p>To override this parameter use ui_time_zone shortcode attribute:</p>
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_time_zone='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_time_zone='new_value' ]");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is an integer number in the range of -12..13 . Use 1000 to indicate that visitor's time zone must be used.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="server_now_date">
<h4 class="dsrc_h4">Use Server Time</h4>
<p>When a target date is selected it is possible to pass the current time with server script. For example you have a new site that is coming soon and want to show remaining time till opening. If you pass the "now" date/time from the server the script will calculate the remaining time for the location of the server. This setting will not take into account the time zone setting, because it is already calculated in the "now" date/time returned from the server. In other words this is one of the two possible ways to set target date/time.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="relative_date">
<h4 class="dsrc_h4">Use Now Offset</h4>
<p>Sometimes another type of countdown is needed, which sets the target date relatively to current time. For example a countdown must count 2 minutes starting when page loads. The code will take your current time zone, will take your current local time and add or substract the amount of offset you desire. The possible units are seconds minutes, hours and days. When using this setting target date, hours, minutes, seconds and time zone will be ignored. This is because that is an alternative way of setting date/time.</p>

<p>To override this setting in the saved countdown use the following shortcode attributes:</p>

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_use_now_offset='new_value1' ui_type_offset='new_value2' 'ui_value_offset'='new_value3' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_use_now_offset='new_value1' ui_type_offset='new_value2' 'ui_value_offset'='new_value3' ]");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value1</span> can be 1 for adding time, or -1 for substracting time.</p>
<p class="dsrc_newvalue"><span>new_value2</span> can be 1000, 60000, 3600000, 86400000, which corresponds to seconds, minutes, hours and days.</p>
<p class="dsrc_newvalue"><span>new_value3</span> is an integer value greater than zero.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="fillStyleSymbol1">
<h4 class="dsrc_h4">Digit Color</h4>
The countdown uses two sets of colors. First set defines how the rightmost group will look like. Depending on showing and hidden groups, the rightmost group may differ. Typically that is the seconds group. Digits have only solid color. The next group uses the second color set. Typically that is the minutes group. 

You can override this setting with shortcode attributes ui_fillStyleSymbol1 and ui_fillStyleSymbol2 like this:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_fillStyleSymbol1='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_fillStyleSymbol1='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is HTML color (#ffffff, #ff0000, etc.)</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="fillStylesPanel_g1_1">
<h4 class="dsrc_h4">Gradient Start</h4>
Each set of colors defines also how the panel behind the digit will look like. It uses a gradient fill, which starts from the center of the panel using the gradient start color. The gradient fill goes in both top and bottom directions of the panel. It ends with gradient end color. If you want solid color, you will need to put same values in both gradient start and gradient end colors.

You can override this setting with shortcode attributes ui_fillStylesPanel_g1_1 and ui_fillStylesPanel_g2_1 like this:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_fillStylesPanel_g1_1='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_fillStylesPanel_g1_1='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is HTML color (#ffffff, #ff0000, etc.)</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="fillStylesPanel_g1_2">
<h4 class="dsrc_h4">Gradient End</h4>
This is the color used to end the gradient fill of panels behind the digits for each group. The gradient uses also the gradient start color and if you want solid colored panels then set same values for both options.

You can override this setting with shortcode attributes ui_fillStylesPanel_g1_2 and ui_fillStylesPanel_g2_2 like this:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_fillStylesPanel_g1_2='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_fillStylesPanel_g1_2='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is HTML color (#ffffff, #ff0000, etc.)</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="set_id">
<h4 class="dsrc_h4">Digits Font</h4>
The digits in the tool are defined using SVG curves. This allows nice looking graphic in both small and large resolutions. Only two sets of digits are available in this tool, but new digits can be added relatively easy. The two existing sets are base on Oswald and Federation fonts.

To override the saved setting use shortcode attribute ui_set_id:

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_set_id='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_set_id='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> can be 0 or 1. Zero stands for "Oswald" and one for "Federation".</p>

<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="pan_id">
<h4 class="dsrc_h4">Panel Behind Digit</h4>
Panels are also SVG defined figures. Only three panels are defined - two wide and one narrow. Use shortcode attribute ui_pan_id to change already saved setting:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_pan_id='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_pan_id='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> can be 0, 1 or 2. These stand for "Thin Rounded", "Wide Squared" and "Wide Rounded".</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="type3d">
<h4 class="dsrc_h4">3D Projection</h4>
The tool is making 3D calculatins in real time and it is possible to set the point of view. Single means that each digit will be animated as if the viewer is directly in front of it. Group means that the visitor is in front of the center of the countdown - so right hand digits will flip to the right and left hand digits will flip to the left. If you want different setting for some of your instances you can use shortcode attribute ui_type3d:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_type3d='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_type3d='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> can be "single" or "group". The "single" projection value uses every digit center to calculate perspective values, while the "group" uses the tool's center.</p>




<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="groups_spacing">
<h4 class="dsrc_h4">Group Spacing</h4>
By default groups are spaced with the amount of space between digits within each group. This setting defines the additional spacing added. Zero means no space is added and integer numbers starting from 1 point how many times the digits space is added to groups space. Use chortcode attribute ui_groups_spacing to override setting saved by the visual editor:

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_groups_spacing='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_groups_spacing='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is of integer type, equal or greater than zero. Zero means no additional spacing will be added.</p>


<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="spacer">
<h4 class="dsrc_h4">Spacer Icon</h4>
Squares, circles or no icons can show between groups. Their color, glow and blur is taken from the label's settings. Override saved setting by using the ui_spacer shortcode attribute:

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_spacer='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_spacer='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> can be "circles", "squares" or "none".</p>


<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="day_digits">
<h4 class="dsrc_h4">Day Digits Number</h4>
Number of digits to represent days remaining. Can also be used in custom mode, where thousands can be spaced with comma sign. To change the digits number for a given instance use shortcode attribute ui_day_digits:

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_day_digits='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_day_digits='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is an integer number greater than zero.</p>


<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="text_color">
<h4 class="dsrc_h4">Label Color</h4>
This color is used for all texts and spacer icons. You can override this setting with shortcode attributes ui_text_color like this:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_text_color='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_text_color='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is HTML color (#ffffff, #ff0000, etc.)</p>

<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="text_glow">
<h4 class="dsrc_h4">Label Glow</h4>
This color is used for the blur effect on texts and spacer icons. You can override this setting with shortcode attributes ui_text_glow like this:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_text_glow='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_text_glow='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is HTML color (#ffffff, #ff0000, etc.)</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="text_blur">
<h4 class="dsrc_h4">Label Blur</h4>
Blur amount of labels and spacers in points. To override this setting for chosen instances use ui_text_blur shortcode attribute:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_text_blur='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_text_blur='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is an integer even number (0,2,4,6,8)</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="font_to_digit_ratio">
<h4 class="dsrc_h4">Label Size</h4>
It is relative to the size of the digits. It gets calculated as a percentage of the current digit size. (10% - 50%). You can override this setting with shortcode attribute ui_font_to_digit_ratio like this:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_font_to_digit_ratio='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_font_to_digit_ratio='new_value']");
?&gt;
</pre>
<p class="dsrc_newvalue"><span>new_value</span> is a float number between 0.1 and 0.5, which stands for 10% to 50% of digit size.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="labels_space">
<h4 class="dsrc_h4">Gap Size</h4>
The space between digits and labels is defined as percentage of the size of the labels. When the tool gets resized the label size changes and the gap also changes accordingly. If you want to change the saved value use the shortcode attribute ui_labels_space:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_labels_space='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_labels_space='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is an float number from 1 to 1.3. One means no space, 1.3 means space, equal to 30% of the label size.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="days_long">

<h4 class="dsrc_h4">Days Long/Short</h4>
This is the text for the days label. When there is enough space, the long text dispays, otherwise the short text displays. The value can be overriden by the usage of shortcode attribute ui_days_long:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_days_long='new_value1' ui_days_short='new_value2' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_days_long='new_value1' ui_days_short='new_value2']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value1, new_value2</span> are of string type.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="hours_long">

<h4 class="dsrc_h4">Hours Long/Short</h4>
This is the text for the hours label. When there is enough space, the long text dispays, otherwise the short text displays. The value can be overriden by the usage of shortcode attribute ui_hours_long:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_hours_long='new_value1' ui_hours_short='new_value2' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_hours_long='new_value1' ui_hours_short='new_value2']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value1, new_value2</span> are of string type.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="mins_long">

<h4 class="dsrc_h4">Minutes Long/Short</h4>
This is the text for the minutes label. When there is enough space, the long text dispays, otherwise the short text displays. The value can be overriden by the usage of shortcode attribute ui_mins_long:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_mins_long='new_value1' ui_mins_short='new_value2' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_mins_long='new_value1' ui_mins_short='new_value2']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value1, new_value2</span> are of string type.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="secs_long">

<h4 class="dsrc_h4">Seconds Long/Short</h4>
This is the text for the seconds label. When there is enough space, the long text dispays, otherwise the short text displays. The value can be overriden by the usage of shortcode attribute ui_secs_long:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_secs_long='new_value1' ui_secs_short='new_value2' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_secs_long='new_value1' ui_secs_short='new_value2']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value1, new_value2</span> are of string type.</p>


<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="show_hide">

<h4 class="dsrc_h4">Show/Hide</h4>
Each group of digits can be hidden. Text labels can also be hidden. There are several shortcode attributes that can be used to override each setting. These are: ui_show_dd, ui_show_hh, ui_show_mm, ui_show_ss for each deigit group and ui_show_labels for text labels.
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_show_dd='new_value1' ui_show_hh='new_value2' ui_show_mm='new_value3' ui_show_ss='new_value4' ui_show_labels='new_value5' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_show_dd='new_value1' ui_show_hh='new_value2' ui_show_mm='new_value3' ui_show_ss='new_value4' ui_show_labels='new_value5']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value1, new_value2, new_value3, new_value4, new_value5</span> can be 0 or 1.</p>


<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="f_family">

<h4 class="dsrc_h4">Labels Font</h4>
Use fonts already installed for your theme. If you want to override this setting use the ui_f_family shortcode attribute:
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_f_family='new_value' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_f_family='new_value']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value</span> is of string type ("Arial", "Verdana", "Courier").</p>



<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<hr class="dsrc_hr" id="f_size">

<h4 class="dsrc_h4">Label Size(min/max)</h4>
Label size is calculated based on the size of each digit, which on its part is calculated based on available space for the tool. Enter integer values to control the maximum and minimum size. To override the saved values you can use the ui_min_f_size and ui_max_f_size shortcode attributes.
<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_min_f_size='new_value1' ui_max_f_size='new_value2' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_min_f_size='new_value1' ui_max_f_size='new_value2']");
?&gt;
</pre>

<p class="dsrc_newvalue"><span>new_value1, new_value2</span> is of integer type.</p>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>


<hr class="dsrc_hr" id="complete">

<h4 class="dsrc_h4">Complete Handler</h4>
When target date is reached the tool can execute a function, which is defined in the complete handler field. It must be a valid Javascript function. The tool will fail to display if there is something wrong with the function. The tool gets destroyed after the function executes. Usually clients want to display a div object, which announces for example the end of a promotion, or other event the countdown shows the remaining time for. For example if you want to show the div with id "time_expired" you need to fill the field like this:


<pre class="dsrc_pre">
function(){$("#time_expired").fadeIn()}
</pre>

To override the saved handler you can use the ui_complete_handler shortcode attribute. For example:

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_complete_handler='function(){alert("Ready!!!")}' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_complete_handler='function(){alert("Ready!!!")}']");
?&gt;
</pre>

<p>Note: The complete handler is not allowed to execute in the admin page.</p>


<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>



<hr class="dsrc_hr">

<h4 class="dsrc_h4">ADVANCED USAGE</h4>
This tool can be used not only as a countdown but also to count time elapsed after a date in the past. This can be done by the usage of ui_target_future shortcode attribute. If you set it to 0 the tool will not execute the complete handler function and will not get destroyed:

<pre class="dsrc_pre">
[dsrc id='<?php echo $post->ID ?>' ui_target_future='0' /]
</pre>

or

<pre class="dsrc_pre">
&lt;?php
echo do_shortcode("[dsrc id='<?php echo $post->ID ?>' ui_target_future='0']");
?&gt;
</pre>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
This Wordpress plugin is built to make possible the usage of jQuery Responsive Countdown with Visual Builder item in sites powered by Wordpress. Wordpress replaces the dsrc shortcodes with some HTML and Javascript code like this:



<pre class="dsrc_pre">
&lt;div id="<?php echo $ui_div_id ?>" style="position: relative; width: 100%; height: 100px;"&gt;&lt;/div&gt;
&lt;script type='text/javascript'&gt;
jQuery(function() {
<?php 	
  echo "&nbsp;&nbsp;".$ui_div_id.' = jQuery("#'.$ui_div_id.'").ResponsiveCountdown({';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'target_date:"'.date('Y', strtotime('+1 year')).'/1/1 0:0:0",paused: false,server_now_date: "'.$server_now_date.'",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'time_zone:'.$time_zone.',target_future:true,';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'set_id:'.$ui_set_id.',pan_id:'.$ui_pan_id.',day_digits:'.$ui_day_digits.',';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'fillStyleSymbol1:"rgba('.$r1.', '.$g1.', '.$b1.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'fillStyleSymbol2:"rgba('.$r2.', '.$g2.', '.$b2.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'fillStylesPanel_g1_1:"rgba('.$rg11.', '.$gg11.', '.$bg11.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'fillStylesPanel_g1_2:"rgba('.$rg12.', '.$gg12.', '.$bg12.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'fillStylesPanel_g2_1:"rgba('.$rg21.', '.$gg21.', '.$bg21.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'fillStylesPanel_g2_2:"rgba('.$rg22.', '.$gg22.', '.$bg22.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'text_color:"rgba('.$rt.', '.$gt.', '.$bt.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'text_glow:"rgba('.$rg.', '.$gg.', '.$bg.', 1)",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'show_ss:'.$ui_show_ss.',show_mm:'.$ui_show_mm.',';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'show_hh:'.$ui_show_hh.',show_dd:'.$ui_show_dd.',';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'f_family:"'.$ui_f_family.'",show_labels:'.$ui_show_labels.',';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'type3d:"'.$ui_type3d.'",max_height:'.$ui_max_height.',';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'days_long:"'.$ui_days_long.'",days_short:"'.$ui_days_short.'",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'hours_long:"'.$ui_hours_long.'",hours_short:"'.$ui_hours_short.'",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'mins_long:"'.$ui_mins_long.'",mins_short:"'.$ui_mins_short.'",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'secs_long:"'.$ui_secs_long.'",secs_short:"'.$ui_secs_short.'",';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'min_f_size:'.$ui_min_f_size.',max_f_size:'.$ui_max_f_size.',';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'spacer:"'.$ui_spacer.'",groups_spacing:'.$ui_groups_spacing.',text_blur: '.$ui_text_blur.',';
	echo "\n&nbsp;&nbsp;&nbsp;&nbsp;".'font_to_digit_ratio:'.$ui_font_to_digit_ratio.',labels_space:'.$ui_labels_space;
	echo "\n&nbsp;&nbsp;".'});';
	echo "\n".'});'."\n";
?>
&lt;/script&gt;
</pre>
<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
You can see that a div element is created with "Container ID" and also a Javascript variable is created with same name. The variable stores the result from the execution of the ResponsiveCountdown method, which does all the work. The jQuery plugin has its options, which differ slightly from the shortcode attributes. The variable makes possible the usage of the jQuery plugin public methods in your Javascript code like this:

<pre class="dsrc_pre">
<?php echo $ui_div_id ?>.pause_anim();
<?php echo $ui_div_id ?>.play_anim();
<?php echo $ui_div_id ?>.destroy_countdown();
<?php echo $ui_div_id ?>.set_options([name1,value1,name2,value2]);
<?php echo $ui_div_id ?>.get_option(name);
<?php echo $ui_div_id ?>.set_custom_state(value);
</pre>

For example you can display one of the countdown options using the complete handler function like this:

<pre class="dsrc_pre">
function(){alert(<?php echo $ui_div_id ?>.get_option("counter_width"))}
</pre>

To learn more about the usage of the jQuery Responsive Countdown with Visual builder and how to use all of its features in your custom Javascript code please read this additional documentation.

<div class="dsrc_note"><a href="#back_to_top">Back To Top <img src="<?php echo plugins_url('ui/images/top.png', __FILE__)?>" /></a></div>
<?php
}
//-------------------------------------------------------------------------------------------------
//handle SAVE event for custom post
add_action('save_post', 'dsrc_save_countdown_info');
function dsrc_save_countdown_info($post_id) {
	global $ui_names;
  // verify nonce
  if (!isset($_POST['dsrc_countdown_box_nonce']) || !wp_verify_nonce($_POST['dsrc_countdown_box_nonce'], basename(__FILE__))) {
      return $post_id;
  }
  // check autosave
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
      return $post_id;
  }
  // check permissions
  if ('dsrc_countdown_type' == $_POST['post_type'] && current_user_can('edit_post', $post_id)) {
			for($i=0;$i<count($ui_names);$i++){
				$$ui_names[$i] = (isset($_POST[$ui_names[$i]]) ? $_POST[$ui_names[$i]] : '');
			}
      $ui_show_dd == "yes" ? $ui_show_dd = 1 : $ui_show_dd = 0;
      $ui_show_hh == "yes" ? $ui_show_hh = 1 : $ui_show_hh = 0;
      $ui_show_mm == "yes" ? $ui_show_mm = 1 : $ui_show_mm = 0;
      $ui_show_ss == "yes" ? $ui_show_ss = 1 : $ui_show_ss = 0;
      $ui_show_labels == "yes" ? $ui_show_labels = 1 : $ui_show_labels = 0;
			$countdown_options = Array();
			for($i=0;$i<count($ui_names);$i++){
				array_push($countdown_options, $$ui_names[$i]);
			}
			$countdown_options = strip_tags(raw_json_encode($countdown_options));
      update_post_meta($post_id, "_dsrc_countdown_options", $countdown_options);
  } else {
      return $post_id;
  }
}
//-------------------------------------------------------------------------------------------------
//for utf8 symbols like Cyrillic letters in labels.
function raw_json_encode($input) {
    function link_code(array $matches) {
        $pack = pack('H*',$matches[1]);
        return mb_convert_encoding($pack,'UTF-8','UTF-16');
    }
    return preg_replace_callback(
        '/\\\\u([0-9a-zA-Z]{4})/',
        'link_code',
        json_encode($input)
    );
//-------------------------------------------------------------------------------------------------
}
?>