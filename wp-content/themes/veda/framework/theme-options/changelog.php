<!-- #changelog -->
<div id="changelog" class="bpanel-content">

    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content no-margin">

        <!-- #tab1-changelog -->
        <div id="tab1" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php echo constant('THEME_NAME'); esc_html_e(' Theme Change Log', 'veda');?></h3>
                </div>
                
                <div class="box-content">
                <pre>
2016.07.21 - version 1.3
 * Added 19 demo sliders (Layer & Revolution)

2016.07.19 - version 1.2
 * Added veda demo importer plugin
 * All importer files & functions moved to plugin (Veda Demo Importer) 
 * Dummy content optimized
 * Some design tweaks updated
 * Core Plugin, Attorney addon updated

2016.07.15 - version 1.1
 * Minor Bpanel options updated
 * Dummy content updated

2016.07.15 - version 1.0
 * First release!</pre>
                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->            
        </div><!--#tab1-import-demo end-->

    </div><!-- .bpanel-main-content end-->
</div><!-- #changelog end-->